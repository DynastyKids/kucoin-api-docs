FROM ruby:2.3.8-jessie

EXPOSE 4567

WORKDIR /root

RUN apt-get update && apt-get install -y --no-install-recommends build-essential git nodejs && gem install bundler -v 1.17.3 && git clone https://github.com/Kucoin/kucoin-api-docs.git && cd /root/kucoin-api-docs && bundle _1.17.3_ install && apt-get remove -y build-essential && apt-get autoremove -y && rm -rf /var/lib/apt/lists/* 

WORKDIR /root/kucoin-api-docs
# RUN bundle _1.17.3_ exec middleman server 
RUN chmod +x /root/kucoin-api-docs/deploy.sh

ENTRYPOINT ["/root/kucoin-api-docs/deploy.sh"]

CMD ["build"]
